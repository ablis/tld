# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [0.3.0](https://gitlab.com/ablis/tld/compare/v0.2.0...v0.3.0) (2020-12-31)


### Features

* re-fetch TLD list periodically ([c2f6aa7](https://gitlab.com/ablis/tld/commit/c2f6aa76efc8dcd0cd7ed9eded2bc93a7a1ff3d2))

## [0.2.0](https://gitlab.com/ablis/tld/compare/v0.1.1...v0.2.0) (2020-12-19)


### Features

* add is_tld?/1 string checker ([90ae76a](https://gitlab.com/ablis/tld/commit/90ae76aa313d7e25d07fd1ce5eee46915c5cde18))
* set up Credo and additional CI checks ([ea46c4e](https://gitlab.com/ablis/tld/commit/ea46c4e0499cace9d5a13bc8c3b647931d09b787))


### Documentation

* add since-metadata to public APIs ([514fd6f](https://gitlab.com/ablis/tld/commit/514fd6f4aefdf226487ebffaabd3eada28940e1a))

### [0.1.1](https://gitlab.com/ablis/tld/compare/v0.1.0...v0.1.1) (2020-12-06)


### Documentation

* add link to changelog ([9e72f25](https://gitlab.com/ablis/tld/commit/9e72f25e1887c1cdc5c83ec91f40a17521cebf83))
* improve documentation ([30c6358](https://gitlab.com/ablis/tld/commit/30c6358a92c0158a89943e1ae7ce522f9ef360e2))

## 0.1.0 (2020-12-06)


### Features

* add get_list() API to get TLD list ([6415669](https://gitlab.com/ablis/tld/commit/64156690427111b1435ae20bf6b0824a614353c6))
* add get_tld_from_domain_name/1 public API ([d3093d9](https://gitlab.com/ablis/tld/commit/d3093d919de84a94a048e1d966a893f975367ba3))
* make TLD list URL configurable ([da76d91](https://gitlab.com/ablis/tld/commit/da76d91b41eee3dd7a51ff19d3710eef00123527))
