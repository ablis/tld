defmodule Tld.HttpcListClient do
  @moduledoc """
  Simple TLD list client, using the BEAM’s built-in `:httpc` library.
  """

  @doc """
  Fetch URL with `:httpc`.
  """
  def fetch(url) do
    case :httpc.request(url) do
      {:ok, {{_http_version, 200, 'OK'}, _headers, body}} -> {:ok, List.to_string(body)}
      metadata -> {:err, metadata}
    end
  end
end
