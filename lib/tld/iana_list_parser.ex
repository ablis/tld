defmodule Tld.IanaListParser do
  @moduledoc """
  Parser for IANA’s Top-Level Domain list.
  """

  @version_matcher ~r/^#+\s+Version\s+(?<version>[0-9]{10}),/

  @doc """
  Parse the body content of the TLD list.
  """
  def parse(body) when is_binary(body) and bit_size(body) > 0 do
    [first_line | tlds] = String.split(body, ["\r", "\n", "\r\n"], trim: true)
    version = parse_version(first_line)

    if is_integer(version) and length(tlds) > 1 do
      {:ok, version, tlds}
    else
      {:err, :invalid_body}
    end
  end

  def parse(_body), do: {:err, :invalid_body}

  @doc """
  Parse the first line of the IANA TLD file, extracting the version number.
  """
  def parse_version(first_line) when is_binary(first_line) do
    case Regex.named_captures(@version_matcher, first_line) do
      %{"version" => version} -> String.to_integer(version)
      _ -> :err
    end
  end
end
