defmodule Tld.DomainName do
  @moduledoc """
  Logic for handling and parsing domain names with respect to Top-Level Domains.
  """

  @doc """
  Match TLD in given domain name to given list of TLDs.
  """
  def match_tld(domain_name, tld_list) do
    tld_list
    |> Enum.filter(&ends_with_tld?(domain_name, &1))
    |> Enum.sort_by(&byte_size/1, :desc)
    |> Enum.at(0)
  end

  defp ends_with_tld?(domain_name, tld) do
    # Special case for just the TLD.
    if domain_name == tld do
      true
    else
      # Add a . before the TLD to ensure the domain ME.TEST does not match the
      # .ST TLD, and other such cases.
      String.ends_with?(domain_name, ".#{tld}")
    end
  end
end
