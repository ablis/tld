defmodule Tld.ListServer do
  @moduledoc """
  GenServer to manage TLD list state.
  """

  use GenServer
  alias Tld.IanaListParser
  require Logger

  defstruct last_fetch_datetime: nil,
            list: [],
            list_url: "https://data.iana.org/TLD/tlds-alpha-by-domain.txt",
            # Almost 12 hours in seconds.
            periodic_fetch_interval: 42_000,
            version: 0

  @impl true
  def init(%{} = state) do
    schedule_periodic_fetch(state)

    {:ok, fetch_list(state)}
  end

  @impl true
  def handle_call({:get_last_fetch_datetime}, _from, %__MODULE__{} = state) do
    {:reply, state.last_fetch_datetime, state}
  end

  @impl true
  def handle_call({:get_list}, _from, %__MODULE__{} = state) do
    {:reply, state.list, state}
  end

  @impl true
  def handle_info(:periodic_fetch, state) do
    schedule_periodic_fetch(state)

    {:noreply, fetch_list(state)}
  end

  @doc """
  Get TLD list from `Tld.ListServer` state.
  """
  def get_list do
    GenServer.call(__MODULE__, {:get_list})
  end

  @doc """
  Standard `start_link/1` function to start `Tld.ListServer` as part of an
  `Application`’s `Supervisor` tree.
  """
  def start_link(_opts) do
    GenServer.start_link(__MODULE__, %__MODULE__{}, name: __MODULE__)
  end

  defp fetch_list(%__MODULE__{} = state) do
    case Tld.HttpcListClient.fetch(state.list_url) do
      {:ok, body} ->
        Logger.info("Succesfully fetched TLD list from IANA.")
        {:ok, version, list} = IanaListParser.parse(body)

        state
        |> Map.put(:version, version)
        |> Map.put(:list, list)
        |> Map.put(:last_fetch_datetime, DateTime.utc_now())

      {:err, _metadata} ->
        Logger.error("Request to load TLD list failed.")
        state
    end
  end

  defp schedule_periodic_fetch(%{periodic_fetch_interval: seconds}) when is_integer(seconds) do
    Process.send_after(self(), :periodic_fetch, seconds * 1000)
  end

  defp schedule_periodic_fetch(_state), do: nil
end
