defmodule TldTest do
  use ExUnit.Case
  doctest Tld

  test "get_list/0" do
    list = Tld.get_list()

    # The number of TLDs will vary with time, but there would likely never come
    # a time where it is less than 100.
    assert length(list) > 100
  end
end
