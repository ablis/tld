defmodule Tld.DomainNameTest do
  use ExUnit.Case, async: true
  alias Tld.DomainName

  @tld_list [
    "AWESOME",
    "COM",
    "CO.UK",
    "UK",
    "ZEBRA.UK",
    "ZZZ"
  ]

  describe "match_tld/2" do
    test "finds TLDs in domain name" do
      assert DomainName.match_tld("ELIXIR.AWESOME", @tld_list) == "AWESOME"
      assert DomainName.match_tld("WWW.SPECTRE.CO.UK", @tld_list) == "CO.UK"
      assert DomainName.match_tld("EAT.ZEBRA.UK", @tld_list) == "ZEBRA.UK"
      assert DomainName.match_tld("WWW.PARLIAMENT.UK", @tld_list) == "UK"
    end
  end
end
