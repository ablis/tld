defmodule Tld.HttpcListClientTest do
  use ExUnit.Case, async: true
  alias Tld.HttpcListClient

  describe "fetch/1" do
    test "successfully fetches www.google.com" do
      assert {:ok, _body} = HttpcListClient.fetch("https://www.google.com/")
    end

    test "fails fetching from a non-existent local server" do
      assert {:err, _metadata} = HttpcListClient.fetch("http://127.0.0.1:62345")
    end
  end
end
