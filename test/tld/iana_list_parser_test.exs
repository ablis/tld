defmodule Tld.IanaListParseTest do
  use ExUnit.Case, async: true
  alias Tld.IanaListParser

  describe "parse/1" do
    test "parses correct IANA list body" do
      body = """
      # Version 2020120500, Last Updated Sat Dec  5 07:07:01 2020 UTC
      AWESOME
      BEAUTIFUL
      CLASSIC
      DIVINE
      EXCELLENT
      FABULOUS
      FANTASTIC
      FIRST-RATE
      """

      {:ok, version, tlds} = IanaListParser.parse(body)
      # credo:disable-for-next-line Credo.Check.Readability.LargeNumbers
      assert version == 2020_12_05_00
      assert Enum.at(tlds, 3) == "DIVINE"
    end

    test "does not crash on invalid input" do
      {:err, :invalid_body} = IanaListParser.parse("")

      fail_body = """
      # Version whatever
      TURBO
      FAIL
      """

      {:err, :invalid_body} = IanaListParser.parse(fail_body)
      {:err, :invalid_body} = IanaListParser.parse(nil)
    end
  end
end
