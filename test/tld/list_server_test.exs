defmodule Tld.ListServerTest do
  use ExUnit.Case, async: true
  alias Tld.ListServer

  describe "ListServer with default settings" do
    test "starts and returns list" do
      {:ok, pid} = GenServer.start_link(ListServer, %ListServer{}, name: :test1)
      list = GenServer.call(pid, {:get_list})

      assert length(list) > 100

      GenServer.stop(pid)
    end
  end

  describe "ListServer with invalid list URL" do
    @moduletag capture_log: true

    test "returns empty list" do
      {:ok, pid} =
        GenServer.start_link(
          ListServer,
          %ListServer{
            list_url: "http://127.0.0.1:63456/nope.txt"
          },
          name: :test2
        )

      list = GenServer.call(pid, {:get_list})

      assert Enum.empty?(list)

      GenServer.stop(pid)
    end
  end

  describe "ListServer with short periodic fetch interval" do
    @moduletag capture_log: true

    test "returns empty list" do
      {:ok, pid} =
        GenServer.start_link(
          ListServer,
          %ListServer{
            periodic_fetch_interval: 1
          },
          name: :test3
        )

      first_dt = GenServer.call(pid, {:get_last_fetch_datetime})

      Process.sleep(1200)

      second_dt = GenServer.call(pid, {:get_last_fetch_datetime})

      assert DateTime.compare(second_dt, first_dt) == :gt

      GenServer.stop(pid)
    end
  end
end
