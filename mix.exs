defmodule Tld.MixProject do
  use Mix.Project

  @gitlab_url "https://gitlab.com/ablis/tld"

  def project do
    [
      app: :tld,
      name: "TLD",
      description: """
      Utility functions to deal with Top-Level Domains of the global DNS system.

      Fetches the current list of TLDs from IANA and makes it accessible with
      a simple function, and offers minor utilties for recognising TLDs in
      domain names.
      """,
      version: "0.3.0",
      elixir: "~> 1.11",
      deps: deps(),
      aliases: aliases(),
      package: [
        maintainers: ["Mikkel Hoegh"],
        licenses: ["Apache-2.0"],
        links: %{
          Changelog: "#{@gitlab_url}/blob/master/CHANGELOG.md",
          GitLab: @gitlab_url,
          Sponsor: "https://www.abl.is/"
        }
      ],
      source_url: @gitlab_url,
      start_permanent: Mix.env() == :prod
    ]
  end

  def application do
    [
      extra_applications: [:inets, :logger, :ssl],
      mod: {Tld.Application, []}
    ]
  end

  defp aliases do
    [
      ci: [
        "format --check-formatted",
        "deps.unlock --check-unused",
        "credo --strict",
        "test --cover --raise"
      ]
    ]
  end

  defp deps do
    [
      {:credo, "~> 1.4", only: [:dev, :test], runtime: false},
      {:ex_doc, "~> 0.23.0", only: :dev, runtime: false}
    ]
  end
end
