module.exports = {
  packageFiles: [
    {
      filename: 'mix.exs',
      updater: '.version-updater-mix.js'
    }
  ],
  bumpFiles: [
    {
      filename: 'mix.exs',
      updater: '.version-updater-mix.js'
    }
  ],
  "types": [
    {"type": "feat", "section": "Features"},
    {"type": "fix", "section": "Bug Fixes"},
    {"type": "chore", "hidden": true},
    {"type": "docs", "section": "Documentation"},
    {"type": "style", "hidden": true},
    {"type": "refactor", "hidden": true},
    {"type": "perf", "hidden": true},
    {"type": "test", "hidden": true}
]
}
